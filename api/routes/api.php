<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/login',function(Request $request)
  {
    return Route::get('/login', 'Auth/LoginController@login');
});
Route::get('/logout',function(Request $request)
  {
    return Route::get('/logout', 'Auth/LogoutController@logout');
});
Route::get('/register',function(Request $request)
  {
    return Route::get('/login', 'Auth/RegisterController@register');
});


// Not Found
Route::fallback(function(){
    return response()->json(['message' => 'Resource not found.'], 404);
});


